# My personal zsh enviroment

Configuración personal de ZSH con esteroides, azúcar, plugins y muchos colores.

¡ATENCION! Se intalarán los siguientes programas:
[ zsh, tmux, lsd, bat, git, wget , vim]

El instalador sólo funciona en sistemas de 64bit

¡NO EJECUTAR COMO SUDO O COSAS RARAS que te cambiaría el entorno de root!
Ejecuta el instalador estando situado dentro de este directorio.
~ $ cd my-personal-zsh-enviroment
~ $ chmod +x personal_zsh_installer.sh
~ $ ./personal_zsh_installer.sh

Si usas otra distro que no sea basada en Arch o Debian, deberás instalar los programas necesarios manualmente.

Antes de ejecutar nada, realiza un backup de tus ficheros de configuracion de TMUX y ZSH si es que tienes.
OJO! Estas configuraciones son personales, yo se lo que hago, yo se lo que hacen! Tú úsalas bajo tu responsabilidad.
