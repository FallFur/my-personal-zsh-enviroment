# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="jonathan"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git zsh-autosuggestions zsh-syntax-highlighting)

# Load oh-my-zsh.sh
source $ZSH/oh-my-zsh.sh

# bash-insulter
if [ -f /etc/bash.command-not-found ]; then
    . /etc/bash.command-not-found
fi

# User configuration

export PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/home/$USER/.local/bin:/usr/share/games:/sbin:/usr/sbin:/usr/local/sbin:/snap/bin:~/.gem/ruby/3.0.0/bin:~/Programs
# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

#Shell options
set -o ignoreeof

#Internal aliases
alias mv='mv -iv'
alias cp='cp -iv'
alias rm='rm -v'
alias cdd='cd ${OLDPWD}'
alias untar='tar -xvf'
alias vi='vim'
alias fullminar='shred --size=10M --iterations=10 -u --verbose --zero --remove'
alias hexdecode='xxd -ps -r ;echo'
alias :q='exit'
alias cls='clear; ls -F'
alias ipconfig='ip -c --brief addr show'
alias disks='echo ${green}"╓───── m o u n t . p o i n t s"${rst}; echo ${green}"╙────────────────────────────────────── ─ ─ "${rst}; lsblk -a; echo ""; echo ${yellow}"╓───── d i s k . u s a g e"; echo "╙────────────────────────────────────── ─ ─ "${rst}; df -h; echo ""; echo ${purple}"╓───── U.U.I.D.s"; echo "╙────────────────────────────────────── ─ ─ "${rst}; lsblk -f;'

#External aliases
alias ls='lsd --group-dirs=first'
alias ll='lsd -l --group-dirs=first'
alias la='lsd -A --group-dirs=first'
alias lla='lsd -Al --group-dirs=first'
alias l='lsd -F --group-dirs=first'

alias cat='/usr/bin/bat'
alias catn='/usr/bin/cat' 

alias blockscreen='xscreensaver-command -lock'
alias navegador_de_la_ostia="firefox &"
alias youtube_m4a="youtube-dl --extract-audio --audio-format mp3"
alias fallfur_lgtb_touch="lolcat -a --speed=150 --freq=0.1"
alias connect="sudo openvpn"

# Hacking extras
function extractports(){
	ports="$(cat $1 | grep -oP '\d{1,5}/open' | awk '{print $1}' FS='/' | xargs | tr ' ' ',')"
	ip_address="$(cat $1 | grep -oP '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}' | sort -u | head -n 1)"
	echo -e "\t[*] IP Address: $ip_address"  >> extractPorts.tmp
	echo -e "\t[*] Open ports: $ports\n"  >> extractPorts.tmp
	echo $ports | tr -d '\n' | xclip -sel clip
	echo -e "${green}[*] Ports copied to clipboard" >> extractPorts.tmp
	cat extractPorts.tmp; rm extractPorts.tmp
}

#Variables
PUBLIC_IP=`wget http://ipecho.net/plain -O - -q ; echo`

# Colores
bold=$(tput bold)		# Bold
red=$bold$(tput setaf 1)	# Red
green=${bold}$(tput setaf 2) 	# Green
yellow=${bold}$(tput setaf 3) 	# Yellow
blue=${bold}$(tput setaf 4) 	# Blue
purple=${bold}$(tput setaf 5) 	# Purple
cyan=${bold}$(tput setaf 6) 	# Cyan
gray=$(tput setaf 7)            # dim white text
white=${bold}$(tput setaf 7) 	# White
rst=$(tput sgr0) 		# Text reset

#AUTO_TMUX SIN ACOPLAR SESIONES
#ONLY ACTIVATE IF YOU HAVE INSTALLED TMUX

#1º Option
#tmux attach &> /dev/null

#2ª Option (better)
if [[ ! $TERM =~ screen ]]; then
	exec tmux
fi

#Auto mount shared folder
#mount -t vboxsf Documents ~/shared

#Banner
#echo "";
#echo "${cyan}  █████▒▄▄▄       ██▓     ██▓      █████▒█    ██  ██▀███  ";
#echo "▓██   ▒▒████▄    ▓██▒    ▓██▒    ▓██   ▒ ██  ▓██▒▓██ ▒ ██▒";
#echo "▒████ ░▒██  ▀█▄  ▒██░    ▒██░    ▒████ ░▓██  ▒██░▓██ ░▄█ ▒";
#echo "░▓█▒  ░░██▄▄▄▄██ ▒██░    ▒██░    ░▓█▒  ░▓▓█  ░██░▒██▀▀█▄  ";
#echo "░▒█░    ▓█   ▓██▒░██████▒░██████▒░▒█░   ▒▒█████▓ ░██▓ ▒██▒";
#echo " ▒ ░    ▒▒   ▓▒█░░ ▒░▓  ░░ ▒░▓  ░ ▒ ░   ░▒▓▒ ▒ ▒ ░ ▒▓ ░▒▓░";
#echo " ░       ▒   ▒▒ ░░ ░ ▒  ░░ ░ ▒  ░ ░     ░░▒░ ░ ░   ░▒ ░ ▒░";
#echo " ░ ░     ░   ▒     ░ ░     ░ ░    ░ ░    ░░░ ░ ░   ░░   ░ ";
#echo "             ░  ░    ░  ░    ░  ░          ░        ░     ${rst}";
echo "							                            	";
echo " ${cyan}~ Wellcome${rst} ${purple}$USER${rst} ${cyan}to your GNU/Linux system, Hacker ⠠⠵${rst}";
echo "							                                ";
echo " ${red}-!-${rst} ${cyan}Public IP: |${rst}${red}$PUBLIC_IP${rst}${cyan}|${rst} ${red}-!-${rst}                          ";
echo "                                                          ";
